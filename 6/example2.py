# Посчитайте количество палиндромов от 1000 до 10000
number = 1000
q = 0
while number != 10000:
    left = number // 100
    right = number % 100
    left = left // 10 + left % 10 * 10
    if left == right:
        q = q + 1
    number = number + 1
print(q)