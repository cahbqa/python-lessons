import tkinter as tk
#from tkmacosx import Button
from tkinter import E, font as tkFont

# Класс с описанием меню
class Menu(tk.Frame):
	def __init__(self, parent, *args, **kwargs):
		super(Menu, self).__init__(*args, **kwargs)
		self.parent = parent
		# Лэйбл с количеством очков
		self.score = tk.Label(self, text=0)
		self.score.pack(side="left")
		# Лэйбл для отображения состояния игры
		self.condition = tk.Label(self, text="Ходит Х")#X turn
		self.condition.pack(side="left", padx=75)
		# Кнопка для перезапуска игры
		self.refresh_button = tk.Button(self, text="ref")
		self.refresh_button.pack(side="right")

	def update_score(self, score):
		self.score.config(text=str(score))

	def update_condition(self, condition_text: str):
		"""
		Функция ``update_condition`` меняет значение строки состояния на ``condition``
		"""
		self.condition.config(text=condition_text)

	def set_game(self, game):
		self.game = game
		self.refresh_button.config(command=self.game.refresh)
		self.parent.update()

class Game(tk.Frame):
	def __init__(self, parent, menu: Menu,  *args, **kwargs):
		super(Game, self).__init__(*args, **kwargs)
		# Parent - 
		self.parent = parent
		self.menu = menu
		self.buttons = [[0,0,0],[0,0,0],[0,0,0]]
		self.count_moves = 0
		#self.button_font = tkFont.Font(size=36)
		for i in range(3):
			for j in range(3):
				self.buttons[i][j] = tk.Button(
					self,
					width = 10,
					height = 7, 
					command= lambda i=i,j=j: self.buttonclicked(i, j),
					highlightbackground = "blue"
					#bg = "green"
					#font=self.button_font
				)
				self.buttons[i][j].grid(row = i, column = j)

	def buttonclicked(self, i, j):
		self.count_moves += 1
		self.menu.update_score(self.count_moves)
		if self.count_moves % 2 == 1:
			self.buttons[i][j].config(text="X", state='disabled', highlightbackground="red")
			self.menu.update_condition("Ходит 0")
		else:
			self.buttons[i][j].config(text="0", state='disabled', highlightbackground="green")
			self.menu.update_condition("Ходит Х")
		self.parent.update()
		self.check_row(i)
		self.check_col(j)
		self.check_diags()
		if self.count_moves == 9:
			self.menu.update_condition("Ничья")

	def check_row(self, row):
		row_values = [x.cget('text') for x in self.buttons[row]]
		row_buttons = [x for x in self.buttons[row]]
		if row_values[0] == row_values[1] and row_values[2] == row_values[1] and row_values[0] != "":
			self.menu.update_condition(f"{row_values[0]} выиграл")
			for button in row_buttons:
				button.config(highlightbackground = "purple")
			self.deactivate_buttons()
	
	def check_col(self, col):
		col_values = [self.buttons[x][col].cget('text') for x in range(3)]
		col_buttons = [self.buttons[x][col] for x in range(3)]
		if col_values[0] == col_values[1] and col_values[2] == col_values[1] and col_values[0] != "":
			self.menu.update_condition(f"{col_values[0]} выиграл")
			for button in col_buttons:
				button.config(highlightbackground = "purple")
			self.deactivate_buttons()
	
	def check_diags(self):
		main_diag_values = [self.buttons[x][x].cget('text') for x in range(3)]
		main_diag_buttons = [self.buttons[x][x] for x in range(3)]
		side_diag_values = [self.buttons[x][2 - x].cget('text') for x in range(3)]
		side_diag_buttons = [self.buttons[x][2 - x] for x in range(3)]
		if main_diag_values[0] == main_diag_values[1] and main_diag_values[2] == main_diag_values[1] and main_diag_values[0] != "":
			self.menu.update_condition(f"{main_diag_values[0]} выиграл")
			for button in main_diag_buttons:
				button.config(highlightbackground="purple")
			self.deactivate_buttons()
		elif side_diag_values[0] == side_diag_values[1] and side_diag_values[2] == side_diag_values[1] and side_diag_values[0] != "":
			self.menu.update_condition(f"{side_diag_values[0]} выиграл")
			for button in side_diag_buttons:
				button.config(highlightbackground="purple")
			self.deactivate_buttons()

	def deactivate_buttons(self):
		for i in range(3):
			for j in range(3):
				self.buttons[i][j].config(state='disabled')

	def refresh(self):
		for i in range(3):
			for j in range(3):
				self.buttons[i][j].config(state='disabled', highlightbackground = "blue")
				self.buttons[i][j].config(text="", state='active' , highlightbackground = "blue")
		self.menu.update_condition("Ходит Х")
		self.count_moves = 0
		self.menu.update_score(0)


root = tk.Tk()
m = Menu(root)
g = Game(root, m)
m.set_game(g)
g.pack(side="top", fill="both", expand=True)
m.pack(side="bottom", fill="both", expand=True, padx=10, pady=10)

root.mainloop()

  




