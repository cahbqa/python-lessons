import arcade

x = 1
arcade.open_window(600, 600, "ANIMA")
def move_circle(delta_time):
	global x
	arcade.start_render()
	if x >= 200:
		x = - x
	x = x + 1
	arcade.draw_circle_filled(300, 300+x, 30+abs(x), arcade.color.RED)
	arcade.finish_render()


arcade.schedule(move_circle, 1/30)

arcade.run()