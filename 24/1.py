import arcade


color = 0
flag = True
arcade.open_window(600,600)
arcade.set_background_color(arcade.csscolor.SKY_BLUE)

polygon = ([230,140], [230,520], [370,520], [370,140])

def gradient(dt):
    global color, flag
    if flag:
        color +=1
    else:
        color -=1
    if color == 255:
        flag = False
    if color == 0:
        flag = True

    arcade.start_render()
    arcade.draw_polygon_filled(polygon, (105, 105, 105))
    arcade.draw_circle_filled(300, 450, 50, (255, 0, 0, color))
    arcade.draw_circle_filled(300, 330, 50, (255,255,0, color))
    arcade.draw_circle_filled(300, 210, 50, (0, 255, 0, color))
    arcade.finish_render()

arcade.schedule(gradient, 1/500)
arcade.run()