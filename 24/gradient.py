import arcade


color = 1
flag = True
arcade.open_window(600, 600)

def gradient(dt):
	global color, flag
	circle_color = (255, 0, color)#, color)
	if flag:
		color += 1
	else:
		color -= 1
	if color == 255:
		flag = False
	if color == 0:
		flag = True
	arcade.start_render()
	arcade.draw_circle_filled(300, 300, 100, circle_color)
	arcade.finish_render()


arcade.schedule(gradient, 1/120)
arcade.run()