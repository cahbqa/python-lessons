import arcade


arcade.open_window(600, 600, "ANIMA")


def smile():
	arcade.draw_circle_filled(300, 300, 100, arcade.color.YELLOW) # Голова
	arcade.draw_circle_filled(250, 320, 32, arcade.color.WHITE) # Левый глаз
	#arcade.draw_circle_outline(250,320, 32, arcade.color.BLACK, 2) # Контур левого
	arcade.draw_circle_filled(250, 320, 10, arcade.color.BLACK) # Левый зрачок
	arcade.draw_circle_filled(350, 320, 32, arcade.color.WHITE) # Правый глаз
	#arcade.draw_circle_outline(350,320, 32, arcade.color.BLACK, 2) # Контур правого
	arcade.draw_circle_filled(350, 320, 10, arcade.color.BLACK) # Правый зрачок

	arcade.draw_arc_outline(300, 300, 100, 100, arcade.color.BLACK, 233, 307, 0)


arcade.start_render()
smile()
arcade.finish_render()
arcade.run()