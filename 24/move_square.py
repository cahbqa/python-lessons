import arcade

x = 1
arcade.open_window(600, 600, "ANIMA")


def move_circle(delta_time):
	global x
	arcade.start_render()
	x = x + 1
	arcade.draw_rectangle_filled(x, 300, 20,20, arcade.color.AERO_BLUE)
	arcade.finish_render()


arcade.schedule(move_circle, 1/30)

arcade.run()