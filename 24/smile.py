import arcade

x = 1
delay = 500
arcade.open_window(600, 600, "ANIMA")

def smile():
	arcade.draw_circle_filled(300, 300, 100, arcade.color.YELLOW) # Голова
	arcade.draw_circle_filled(250, 320, 32, arcade.color.WHITE) # Левый глаз
	#arcade.draw_circle_outline(250,320, 32, arcade.color.BLACK, 2) # Контур левого
	arcade.draw_circle_filled(250, 320, 10, arcade.color.BLACK) # Левый зрачок
	arcade.draw_circle_filled(350, 320, 32, arcade.color.WHITE) # Правый глаз
	#arcade.draw_circle_outline(350,320, 32, arcade.color.BLACK, 2) # Контур правого
	arcade.draw_circle_filled(350, 320, 10, arcade.color.BLACK) # Правый зрачок


def move_circle(delta_time):
	global x, delay
	x += 5

	arcade.start_render()
	smile()
	# Закрываем глаз
	if x >= delay and x <= delay + 64:
		arcade.draw_rectangle_filled(250,320 + (64 + delay - x), 64, 64, arcade.color.YELLOW)
		arcade.draw_circle_outline(300,300, 200, arcade.color.BLACK, 100)
	# Открываем глаз
	if x >= delay + 64:
		arcade.draw_rectangle_filled(250,320 - (64 + delay - x), 64, 64, arcade.color.YELLOW)
		arcade.draw_circle_outline(300,300, 200, arcade.color.BLACK, 100)
	# Сбрасываем счётчик
	if x >= delay + 64 * 2:
		x = 0
	arcade.finish_render()


arcade.schedule(move_circle, 1/30)

arcade.run()