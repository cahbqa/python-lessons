import arcade
import random

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 800
SPEED = 5

class Game(arcade.Window):
	def __init__(self, width, height, title):
		super().__init__(width, height, title, fullscreen=True)

	def load_map(self, path):
		with open(path, 'r') as file:
			self.maze_width, self.maze_height = list(map(int, file.readline().split()))
			words = file.readline().split()
			self.word = random.choice(words)
			self.word_symbols = list(self.word)
			random.shuffle(self.word_symbols)
			print(self.word)
			raw_maze = file.readlines()
			self.maze = []
			for row in raw_maze:
				self.maze.append(list(map(int, row.split())))
	
	def draw_map(self):
		tile_width = SCREEN_WIDTH // self.maze_width #self.width // self.maze_width
		tile_height = SCREEN_HEIGHT // self.maze_height#self.height // self.maze_height
		colors = [arcade.color.WHITE, arcade.color.RED, arcade.color.GREEN, arcade.color.YELLOW, arcade.color.MAGENTA]
		for y in range(len(self.maze)):
			for x in range(len(self.maze[y])):
				arcade.draw_xywh_rectangle_filled(
					x * tile_width, 
					(len(self.maze[y]) - y - 1) * tile_height, 
					tile_width, 
					tile_height, 
					colors[self.maze[y][x]])
				if self.maze[y][x] == 2:
					self.player_x = x
					self.player_y = y

	def setup(self, path):
		self.load_map(path)

	def on_draw(self):
		arcade.start_render()
		self.draw_map()

	def on_key_press(self, key, modifiers):
		if key == arcade.key.UP:
			if self.maze[self.player_y -1][self.player_x] == 0:
				self.maze[self.player_y -1][self.player_x] = 2
				self.maze[self.player_y][self.player_x] = 0
				self.player_y -= 1
				
		if key == arcade.key.DOWN:
			pass
		if key == arcade.key.LEFT:
			pass
		if key == arcade.key.RIGHT:
			pass
		if key == arcade.key.ESCAPE:
			self.close()



win = Game(SCREEN_WIDTH, SCREEN_HEIGHT, "GAME")
win.setup("maps/1.maze")
arcade.run()