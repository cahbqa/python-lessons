from turtle import *
from math import sin, radians, tan


def roof(angle, length):
	forward(length)
	left((180+angle)// 2)
	forward((length/2)/sin(radians(angle / 2)))
	left(180-angle)
	forward((length/2)/sin(radians(angle / 2)))
	left((180+angle)// 2)


def poly(num):
	angle = 360 / num
	for i in range(num):
		forward(100)
		left(angle)

def house():
	color("red")
	begin_fill()
	roof(120, 100)
	end_fill()
	color("purple")
	begin_fill()
	right(90)
	poly(4)
	end_fill()

def pine():
	color("brown")
	left(90)
	# Ствол дерева
	begin_fill()
	forward(20)
	left(90)
	fd(50)
	left(90)
	fd(20)
	left(90)
	fd(50)
	end_fill()

	up()
	left(180)
	fd(50)
	left(90)
	fd(40)
	right(180)
	down()
	color("green")
	begin_fill()
	roof(30, 100)
	end_fill()
	up()
	right(90)

def sun():
	color("yellow")
	begin_fill()
	circle(30)
	end_fill()
	left(90)
	fd(30)

	for i in range(10):
		up()
		right(36)
		#print(i*36)
		if i % 2 == 0:
			l = 10
		else:
			l = 15
		fd(30 + 10)
		fd(tan(radians(75))*(l//2))
		right(90)
		fd(5)
		left(180)
		down()

		begin_fill()
		roof(30, l)
		up()
		end_fill()

		fd(5)
		left(90)
		fd(30 + 10)
		fd(tan(radians(75))*(l//2))
		left(180)


def draw_back(x1, y1, x2, y2, col):
	up()
	color(col)
	goto(x1,y1)
	down()
	begin_fill()
	goto(x2, y1)
	goto(x2, y2)
	goto(x1, y2)
	goto(x1, y1)
	end_fill()
	up()


def cloud():
	up()
	pass

#house()
#roof(60)
#pine()
#speed(100)
#sun()
#input()