from turtle import *

def goto_next(num):
	color("white")
	forward(num)
	color("black")

def draw_c(a):
	forward(a)
	back(a)
	left(90)
	forward(a)
	right(90)
	forward(a)
	back(a)
	left(90)
	back(a)
	right(90)
	forward(a)

def draw_a(a):
	left(90)
	forward(a)
	right(90)
	forward(a)
	right(90)
	forward(a)
	back(a//2)
	left(90)
	back(a)
	left(90)
	back(a//2)
	right(90)

def draw_sh(a):
	forward(a)
	left(90)
	forward(a)
	back(a)
	left(90)
	forward(a//2)
	right(90)
	forward(a)
	back(a)
	left(90)
	forward(a//2)
	right(90)
	forward(a)
	back(a)
	right(90)
	forward(a)



speed(50)
goto_next(-30 * 3)
draw_c(30)
#color("green")
goto_next(6)
draw_a(30)
goto_next(36)
draw_sh(30)
goto_next(6)
draw_a(30)



input()