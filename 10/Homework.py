#Задача 1: Написать функцию, которая будет печатать все делители передаваемого в
#  функцию аргумента и возвращать их количество
#Задача 2*: Написать функцию, которая будет принимать 
# три аргумента(дата, месяц, год) и возвращать день недели переданной даты

def print_dels(num):
	q = 0
	for i in range(1, num + 1):
		if num % i == 0:
			print(i)
			q += 1
	return q

#d + ((13*m - 1) // 5 ) + y + (y //4 + c // 4 - 2*c + 777),

#где d — число месяца, m — номер месяца, если начинать счет с марта, 
# как это делали в Древнем Риме (март — 1, апрель — 2, ..., февраль — 12), 
# y — номер года в столетии, c — количество столетий.

#Если потом вычислить остаток от деления на 7, то мы получим день недели: 
# 1 — понедельник, 2 — вторник, ..., 6 — суббота, 0 — воскресенье.

def get_day_of_week(d, m, y):
	m = m - 2
	if m <= 0:
		m += 12
	c = y // 100
	y = y % 100
	day_of_week = (d + ((13 * m - 1) // 5 ) + y + (y //4 + c // 4 - 2*c + 777)) % 7
	return day_of_week


#def turn_zero(num):
#	num = 0
#	print("num =", num)
#a = 7
#turn_zero(a)
#print("a =", a)






