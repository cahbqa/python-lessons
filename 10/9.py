from turtle import *

def hex(length):
	for i in range(6):
		forward(length)
		left(60)

def poly(num):
	angle = 360 / num
	for i in range(num):
		forward(100)
		left(angle)

color("white")
back(200)
right(90)
forward(200)
left(90)
#hex(100)
color("red")
poly(9)
input()