from turtle import *

t = Turtle()
sp = 10

def go_up():
	t.goto(t.xcor(), t.ycor() + sp)

def go_down():
	t.goto(t.xcor(), t.ycor() - sp)

def go_left():
	t.goto(t.xcor() - sp, t.ycor())

def go_right():
	t.goto(t.xcor() + sp, t.ycor())


scr = t.getscreen()
scr.onkey(go_up, "Up")
scr.onkey(go_down, "Down")
scr.onkey(go_left, "Left")
scr.onkey(go_right, "Right")
scr.listen()
mainloop()