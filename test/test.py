class A:
	def __init__(self, a, b):
		self.a = a
		self.b = b

class Pet:
	def __init__(self, name, height, age):
		self.name = name
		self.height = height
		self.age = age
		self.cap = height // age
	
	def __add__(self, other):
		if type(other) is list:
			d = dict()
			d['name'] = self.name
			d['height'] = self.height
			other.append(d)
			return other


a = []
cat = Pet('Zoza', 123, 1)
a = cat + a
print(a)