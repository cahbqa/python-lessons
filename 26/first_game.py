import arcade

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 1000
SPEED = 5

class Player(arcade.Sprite):
	def update(self):
		self.center_x += self.change_x
		self.center_y += self.change_y

		if self.left < 1:
			self.left = 1
		if self.right > SCREEN_WIDTH - 1:
			self.right = SCREEN_WIDTH -1
		if self.top > SCREEN_HEIGHT - 1:
			self.top = SCREEN_HEIGHT - 1
		if self.bottom < 1:
			self.bottom = 1


class Game(arcade.Window):
	def __init__(self, width, height, title):
		super().__init__(width, height, title, fullscreen=True)
		self.player_list = None
		self.player_sprite = None
	
	def setup(self):
		self.player_list = arcade.SpriteList()
		self.player_sprite = Player(":resources:images/animated_characters/female_person/femalePerson_idle.png", 0.5)
		self.player_sprite.center_x = 500
		self.player_sprite.center_y = 500
		self.player_list.append(self.player_sprite)

	def on_draw(self):
		arcade.start_render()
		self.player_list.draw()

	def on_update(self, delta_time):
		self.player_list.update()

	def on_key_press(self, key, modifiers):
		if key == arcade.key.UP:
			self.player_sprite.change_y = SPEED
		if key == arcade.key.DOWN:
			self.player_sprite.change_y = -SPEED
		if key == arcade.key.LEFT:
			self.player_sprite.change_x = -SPEED
		if key == arcade.key.RIGHT:
			self.player_sprite.change_x = SPEED
		if key == arcade.key.ESCAPE:
			self.close()

	def on_key_release(self, key, modifiers):
		if key == arcade.key.UP or key == arcade.key.DOWN:
			self.player_sprite.change_y = 0
		if key == arcade.key.LEFT or key == arcade.key.RIGHT:
			self.player_sprite.change_x = 0


win = Game(SCREEN_WIDTH, SCREEN_HEIGHT, "GAME")
win.setup()
arcade.run()