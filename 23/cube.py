import arcade

arcade.open_window(600, 600, "Drawing Example", resizable=False)

arcade.set_background_color(arcade.csscolor.ROSY_BROWN)


front = [(100,100), (100,400), (400, 400), (400, 100)]
top = [(100, 400), (200, 500), (500, 500), (400, 400)]
right = [(400, 100), (400, 400), (500, 500), (500, 200)]

arcade.start_render()
arcade.draw_polygon_filled(front, (222, 162, 222))
arcade.draw_polygon_filled(top, (207, 124, 207))
arcade.draw_polygon_filled(right, (193, 84, 193))
arcade.finish_render()

arcade.run()