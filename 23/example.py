import arcade

arcade.open_window(600, 600, "Drawing Example", resizable=False)

arcade.set_background_color(arcade.csscolor.ROSY_BROWN)

arcade.start_render()

# draw rectangles
arcade.draw_lrtb_rectangle_filled(0, 300, 350, 300, arcade.csscolor.GREEN)
arcade.draw_rectangle_filled(0,300,10, 40, arcade.csscolor.RED)
# draw circles 
arcade.draw_circle_filled(100,100, 10, (255,0,0))
arcade.draw_circle_outline(200,200,100, (255,0,0), 30)
# draw ellipses
arcade.draw_ellipse_filled(300,300, 100, 50, (255,0,0))
arcade.draw_ellipse_outline(300,300, 100, 50, (0,255,0), 10)


arcade.finish_render()

arcade.run()