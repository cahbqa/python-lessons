import arcade

arcade.open_window(600, 600, "Drawing Example", resizable=False)

arcade.set_background_color(arcade.csscolor.ROSY_BROWN)

arcade.start_render()


for i in range(120):
	col = (255, i % 255, i % 255)
	arcade.draw_circle_filled(300, 300, 300 - i, col)

arcade.finish_render()

arcade.run()