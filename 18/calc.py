import tkinter as tk

window = tk.Tk()

result_field = tk.Entry(state="disabled")
result_field.grid(row=0, column=0, columnspan=4)

for i in range(1, 4):
	for j in range(0, 3):
		b_text = str((i - 1) * 3 + j + 1)
		button = tk.Button(window, text=b_text)#, command=lambda: print(b_text))#result_field.config(text=result_field.cget("text")+b_text))
		button.grid(row=i, column=j)

plus_button = tk.Button(window, text="+")
plus_button.grid(row=1, column=3)
minus_button = tk.Button(window, text="-")
minus_button.grid(row=2, column=3)
mult_button = tk.Button(window, text="*")
mult_button.grid(row=3, column=3)
div_button = tk.Button(window, text="/")
div_button.grid(row=4, column=3)

window.mainloop()
