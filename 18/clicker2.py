import tkinter as tk
from tkinter import font as tkFont

def on_click():
	score = int(button1.cget("text")) + 1
	button1.config(text=str(score))

win = tk.Tk()
helv36 = tkFont.Font(family='Helvetica', size=72, weight='bold')
button1 = tk.Button(win, text="0", command=on_click, width=100, height=100, font=helv36)
button1.pack()
win.mainloop()