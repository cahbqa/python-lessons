/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kmumm <kmumm@student.21-school.ru>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 18:32:49 by kmumm             #+#    #+#             */
/*   Updated: 2022/03/23 18:36:26 by kmumm            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "unistd.h"

void	ft_putstring(char *str)
{
	while (*str)
		write(1, str++, 1);
}

int	main(void)
{
	ft_putstring("Hello world!!!\n");
	return (0);
}
