from turtle import *
from random import randint

colors = ["red", "green", "blue", "yellow"]

def draw_triangle(length, col):
	left(60)
	color(col)
	fd(length)
	right(120)
	fd(length)
	right(120)
	fd(length)
	left(180)
	fd(length)

def draw_n_triangles(n_triangles, length):
	for i in range(n_triangles):
		draw_triangle(length, colors[randint(0, len(colors) - 1)])

speed(50)
draw_n_triangles(50, 10)
#draw_triangle(100)
input()