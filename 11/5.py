from turtle import *

def draw_cube():
	for i in range(4):
		forward(100)
		left(90)

color("purple")
draw_cube()
up()
fd(200)
down()
draw_cube()
input()