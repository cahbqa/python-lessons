import tkinter as tk
#from tkmacosx import Button
from tkinter import E, font as tkFont

class Game(tk.Frame):
	def __init__(self, parent, menu,  *args, **kwargs):
		super(Game, self).__init__(*args, **kwargs)
		# Parent - 
		self.parent = parent
		self.menu = menu
		self.buttons = [[0,0,0],[0,0,0],[0,0,0]]
		self.count_moves = 0
		#self.button_font = tkFont.Font(size=36)
		for i in range(3):
			for j in range(3):
				self.buttons[i][j] = tk.Button(
					self,
					width = 10,
					height = 7, 
					command= lambda i=i,j=j: self.buttonclicked(i, j),
					highlightbackground = "blue"
					#bg = "green"
					#font=self.button_font
				)
				self.buttons[i][j].grid(row = i, column = j)

	def buttonclicked(self, i, j):
		self.count_moves += 1
		self.menu.update_score(self.count_moves)
		if self.count_moves % 2 == 1:
			self.buttons[i][j].config(text="X", state='disabled', highlightbackground="red")
		else:
			self.buttons[i][j].config(text="0", state='disabled', highlightbackground="green")
		self.parent.update()
		self.check_row(i)
		self.check_col(j)
		self.check_diags()
		if self.count_moves == 9:
			print("No one is wins")
			exit(0)

	def check_row(self, row):
		row_values = [x.cget('text') for x in self.buttons[row]]
		if row_values[0] == row_values[1] and row_values[2] == row_values[1] and row_values[0] != "":
			print(row_values[0], "is win")
			exit(0)
	
	def check_col(self, col):
		col_values = [self.buttons[x][col].cget('text') for x in range(3)]
		if col_values[0] == col_values[1] and col_values[2] == col_values[1] and col_values[0] != "":
			print(col_values[0], "is win")
			exit(0)
	
	def check_diags(self):
		main_diag_values = [self.buttons[x][x].cget('text') for x in range(3)]
		side_diag_values = [self.buttons[x][2 - x].cget('text') for x in range(3)]
		if main_diag_values[0] == main_diag_values[1] and main_diag_values[2] == main_diag_values[1] and main_diag_values[0] != "":
			print(main_diag_values[0], "is win")
			exit(0)
		elif side_diag_values[0] == side_diag_values[1] and side_diag_values[2] == side_diag_values[1] and side_diag_values[0] != "":
			print(side_diag_values[0], "is win")
			exit(0)

	def refresh(self):
		for i in range(3):
			for j in range(3):
				self.buttons[i][j].config(text="", state='active', highlightbackground = "blue")
				self.buttons[i][j].config(highlightbackground = "blue")
		self.count_moves = 0
		self.menu.update_score(0)

# Класс с описанием меню
class Menu(tk.Frame):
	def __init__(self, parent, *args, **kwargs):
		super(Menu, self).__init__(*args, **kwargs)
		self.parent = parent
		self.score = tk.Label(self, text=0)
		self.score.pack(side="left")
		self.refresh_button = tk.Button(self, text="ref")
		self.refresh_button.pack(side="right")

	def update_score(self, score):
		self.score.config(text=str(score))

	def set_game(self, game):
		self.game = game
		self.refresh_button.config(command=self.game.refresh)
		self.parent.update()

root = tk.Tk()
m = Menu(root)
g = Game(root, m)
m.set_game(g)
g.pack(side="top", fill="both", expand=True)
m.pack(side="bottom", fill="both", expand=True, padx=10, pady=10)

root.mainloop()

  




