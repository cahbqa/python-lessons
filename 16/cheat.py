# Операции ввода-вывода
a = input() # Вводится строка
print(a)

# Операции с числами
a = int(input()) # int - делает из строки число
b = int(input())
print(a + b)
print(a - b)
print(a * b)
print(a // b) # // - целочисленное деление
print(a / b) # / - деление с остатком, результат - дробное число
print(a % b) # % остаток от деления
print(a ** b) # ** - возведение в степеь

# Операции с строками
a = input()
b = input()
print(a + b)
print(a * 5) # Строку можно умножить на число
print(a.lower())

# Логический тип данных
a = True
b = False
print(7 > 5) # Результат операций сравнения имеет логический тип
print(5 == 4)
print(3 <= 4)

# Условный оператор
#if условие:
#	действия
if input() == "yes":
	print("Првиет!")
	a = int(input())
	print(a ** 5)

a = int(input())
if a < 0:
	print("Отрицательное")
else: # Иначе
	print("Не отрицательное")


a = int(input())
if a % 2 == 0:
	print("Делится на 2")
elif a % 3 == 0:
	print("Делится на 3")
elif a % 5 == 0:
	print("Делится на 5")
else:
	print("Не делится на 2, 3 и 5")


# Оператор while
i = 0
while i < 10:
	print(i * 2)
	i += 1 # Тоже самое что и i = i + 10

# Оператор for
for i in range(10):
	print(i * 2)

for i in "abcde":
	print(i * 5)

# Функции
def hello():
	a = 10
	b = 100
	c = a + b
	d = input()
	#print("Hello")

def hello(a):
	print("Hello" * a)

hello(12)

def hello():
	print(1234)
	a = 123
	return a * "Hello"

a = hello()

b = 10
def pow(a, n):
	a = a ** n
	print(a)