from turtle import *
from random import randint


def start(turtle, x, y, color):
	turtle.up()
	turtle.goto(x,y)
	turtle.shape("turtle")
	turtle.color(color)
	turtle.left(90)

t1 = Turtle()
t2 = Turtle()

start(t1, -20, -200, "pink")
start(t2, 20, -200, "orange")

finish = 200
while t1.ycor() < finish and t2.ycor() < finish:
	t1.fd(randint(4,7))
	t2.fd(randint(4,7))

def dance(turtle):
	for j in range(64):
		turtle.left(30)

winner = t1 if t1.ycor() > t2.ycor() else t2
dance(winner)

#max_t = max(t1.ycor(), t2.ycor())
#if max_t == t1.ycor() and max_t == t2.ycor():
#	dance(t1)
#	dance(t2)
#elif max_t == t1.ycor():
#	dance(t1)
#else:
#	dance(t2)

# В конце забега победившая черепашка далжна начать танцевать
#if t1.ycor() < t2.ycor():
#	dance(t2)
#elif t1.ycor() > t2.ycor():
#	dance(t1)
#else:
#	dance(t1)
#	dance(t2)

mainloop()