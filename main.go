package main

import (
	"fmt"
	//"encoding/json"
)

func readTask() (interface{}, interface{}, interface{}) {
	var first, second float64
	var third string
	fmt.Scan(&first, &second, &third)
	return first, second, third
}

func main() {
	var first, second, third interface{}

	first, second, third = readTask()
	f_oper, ok := first.(float64)
	if !ok {
		fmt.Printf("value=%v: %T", first, first)
		return
	}
	s_oper, ok := second.(float64)
	if !ok {
		fmt.Printf("value:%v: %T", second, second)
		return
	}
	operand, ok := third.(string)
	if !ok {
		fmt.Print("неизвестная операция")
		return
	}
	switch operand {
	case "+":
		fmt.Printf("%.4f", f_oper+s_oper)
	case "-":
		fmt.Printf("%.4f", f_oper-s_oper)
	case "*":
		fmt.Printf("%.4f", f_oper*s_oper)
	case "/":
		fmt.Printf("%.4f", f_oper/s_oper)
	default:
		fmt.Print("неизвестная операция")
	}
	return
}
