import tkinter as tk

from numpy import tile

class ExtraApp(tk.Frame):
	def __init__(self, parent, *args, **kwargs):
		super().__init__(parent, *args, **kwargs)
		for i in range(10):
			tk.Button(parent, text=f"{i}").pack()


class MainApp(tk.Frame):
	def __init__(self, parent,  *args, **kwargs):
		super().__init__(parent, *args, **kwargs)
		self.parent = parent

		self.label = tk.Label(parent, text="Hello, world!")
		self.label.pack()
		self.button1 = tk.Button(parent, text="Hello, world!", command=self.label.pack_forget)
		self.button1.pack()
		self.button2 = tk.Button(parent, text="Add window", command=self.New_window)
		self.button2.pack()
		self.button3 = tk.Button(parent, text="Destroy windows", command=self.delete_all_windows)
		self.button3.pack()
		
		self.windows = []
		self.windows_counter = 0

	def New_window(self):
		self.windows.append(tk.Tk())
		#tk.Label(self.windows[self.windows_counter], text=f"{self.windows_counter}").pack()
		if self.windows_counter % 2 == 0:
			tk.Label(self.windows[self.windows_counter], text=f"{self.windows_counter}").pack()
			tk.Button(self.windows[self.windows_counter], text="destroy", command=self.windows[self.windows_counter].destroy).pack()
		else:
			ExtraApp(self.windows[self.windows_counter])
		#self.windows[-1].mainloop()
		self.windows_counter += 1
		#self.nw = tk.Tk()
		#self.nw.title = "123"
		#self.nw.mainloop()

	def delete_all_windows(self):
		for window in self.windows:
			try:
				window.destroy()
			except:
				pass
		#self.windows_counter = -1

root = tk.Tk(screenName="AAA")
ma = MainApp(root)
ma.pack()
root.mainloop()
