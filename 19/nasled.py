

class Pet():
	def __init__(self, name):
		self.name = name

	def getname(self):
		return self.name

	def rename(self, name):
		self.name = name



class Dog(Pet):
	def __init__(self, name):
		super().__init__(name)

	def bark(self):
		print("Gav!")

	def __add__(self, other):
		return MiniDog(self.name[:len(self.name)//2] + other.name[len(other.name)//2:])


class MiniDog(Pet):
	def __init__(self, name):
		super().__init__(name)

	def minibark(self):
		print("Tyav!")

Murzik = Pet("Murzik")
Tuzik = Dog("Tuzik")
Kukuruzka = Dog("Kukuruzka")
New_dog = Tuzik + Kukuruzka
#Tuzik.name = "new name"
#print(Tuzik.name)
#Tuzik.rename("new name")
#print(Tuzik.getname())
print(New_dog.getname())