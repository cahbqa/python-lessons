import arcade



SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = "My new game"

RECT_WIDTH = 50
RECT_HEIGHT = 50

def on_draw(delta_time):
    arcade.start_render()

    arcade.draw_rectangle_filled(on_draw.center_x, on_draw.center_y,
                                 RECT_WIDTH, RECT_HEIGHT,
                                 arcade.color.ALIZARIN_CRIMSON)

    on_draw.center_x += on_draw.delta_x * delta_time

    if on_draw.center_x < RECT_WIDTH // 2 or on_draw.center_x > SCREEN_WIDTH - RECT_WIDTH // 2:
        on_draw.delta_x *= -1

def draw(delta_time):
	print(delta_time)

on_draw.center_x = 100
on_draw.center_y = 300
on_draw.delta_x = 115

arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
arcade.set_background_color(arcade.color.WHITE)

arcade.schedule(draw, 1)

arcade.run()