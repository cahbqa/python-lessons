import arcade


WIDTH = 600
HEIGHT = 600
SQUARE_CENTER_X = 300
SQUARE_CENTER_Y = 300
SQUARE_WIDTH = 50
SQUARE_HEIGHT = 50
DELTA = 1

arcade.open_window(WIDTH, HEIGHT, "LEFT-RIGHT")


def draw(delta_time):
    global SQUARE_CENTER_X, DELTA

    SQUARE_CENTER_X += DELTA
    if SQUARE_CENTER_X + SQUARE_WIDTH//2 >= WIDTH or SQUARE_CENTER_X - SQUARE_WIDTH//2 <= 0:
        DELTA *= -1

    arcade.start_render()
    arcade.draw_rectangle_filled(
        center_x=SQUARE_CENTER_X,
        center_y=SQUARE_CENTER_Y,
        width=SQUARE_WIDTH,
        height=SQUARE_HEIGHT,
        color=arcade.color.AERO_BLUE)
    arcade.finish_render()


arcade.schedule(draw, 1/120)
arcade.run()
