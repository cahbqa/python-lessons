from re import T
import arcade
from random import randint
from math import sin, cos, pi

SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 1200
CIRCLE_R = 50
CIRCLE_CENTER_X = 600
CIRCLE_CENTER_Y = 600
ANGLE = 78 #randint(0, 360)
TRAECTORY = []
SPEED = 100
ACCSELERATION = -0.1


arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "POOL")

def move(x, y, angle, speed):
	new_x = x + sin(pi/180*angle) * speed
	new_y = y + cos(pi/180*angle) * speed
	return new_x, new_y

def draw(dt):
	global CIRCLE_CENTER_X, CIRCLE_CENTER_Y, CIRCLE_R, ANGLE, TRAECTORY, ACCSELERATION, SPEED
	SPEED += ACCSELERATION
	if SPEED <= 0:
		SPEED = 0
	new_x, new_y = move(CIRCLE_CENTER_X, CIRCLE_CENTER_Y, ANGLE, SPEED)
	if new_x + CIRCLE_R >= SCREEN_WIDTH or new_x - CIRCLE_R <= 0:
		ANGLE = 360 - ANGLE
	if new_y + CIRCLE_R >= SCREEN_HEIGHT or new_y - CIRCLE_R <= 0:
		if ANGLE <= 180:
			ANGLE = 180 - ANGLE
		else:
			ANGLE = 540 - ANGLE
	TRAECTORY.append([new_x, new_y])
	CIRCLE_CENTER_X = new_x
	CIRCLE_CENTER_Y = new_y
	arcade.start_render()
	arcade.draw_line_strip(TRAECTORY, arcade.color.RED)
	#arcade.draw_points(TRAECTORY, arcade.color.GREEN)
	arcade.draw_circle_filled(
		CIRCLE_CENTER_X,
		CIRCLE_CENTER_Y, 
		CIRCLE_R, 
		arcade.color.AERO_BLUE
	)
	arcade.finish_render()
	
arcade.schedule(draw, 1/90)
arcade.run()