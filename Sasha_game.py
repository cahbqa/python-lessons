import tkinter as tk

class App(tk.Frame):
	def __init__(self, master):
		super().__init__(master)
		self.pack()

		self.player_entry = tk.Entry()
		self.player_entry.pack()
		self.bot_entry = tk.Entry(state='disabled')
		self.bot_entry.pack()

		self.player_word = tk.StringVar()
		self.player_word.set("this is a variable")
		self.player_entry["textvariable"] = self.player_word
		self.player_entry.bind('<Key-Return>', self.round)

		self.bot_word = tk.StringVar()
		self.bot_word.set("")

	def round(self, event):
		pass

root = tk.Tk()
myapp = App(root)
myapp.mainloop()
